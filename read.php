<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

include '../../essentials/connection.php';

$conn = new Connection();

$response= array();
$to_decode = json_decode(file_get_contents("php://input"));



$select = "SELECT * FROM users_table;";
$result = mysqli_query($conn->connect(),$select);

if($result){
    $i=0;
    while($row=mysqli_fetch_assoc($result)){
        $response[$i]['Id'] = $row['Id'];
        $response[$i]['Username'] = $row['Username'];
        $response[$i]['Name'] = $row['Name'];
        $response[$i]['Address'] = $row['Address'];
        $response[$i]['Contacts'] = $row['Contacts'];
        $i++;
    }
    echo json_encode($response,JSON_PRETTY_PRINT);
}
else {
    http_response_code(403);
    echo json_encode(array('msg' => 'Failed. Try again!', 'response' => mysqli_error($conn->connect())));
}

$conn->close($conn->connect());
?>